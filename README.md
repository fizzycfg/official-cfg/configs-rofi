# Rofi Configuration

Rofi configuration (fizzy-compliant).

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-rofi/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
